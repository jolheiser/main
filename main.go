package main

import (
	"fmt"
	"os"
	"strings"

	"code.gitea.io/sdk/gitea"
	"github.com/AlecAivazis/survey/v2"
	"github.com/urfave/cli/v2"
	"go.jolheiser.com/beaver"
	"go.jolheiser.com/beaver/color"
)

func main() {
	app := cli.NewApp()
	app.Name = "main"
	app.Usage = "Change your default branch and move over protections"
	app.Action = doMain
	app.Flags = []cli.Flag{
		&cli.BoolFlag{
			Name:  "verbose",
			Usage: "Show extra information",
		},
	}

	if err := app.Run(os.Args); err != nil {
		beaver.Fatal(err)
	}
}

func doMain(ctx *cli.Context) error {
	if ctx.Bool("verbose") {
		beaver.Console.Level = beaver.DEBUG
	}

	questions := []*survey.Question{
		{
			Name:     "token",
			Prompt:   &survey.Input{Message: "Gitea Token"},
			Validate: survey.Required,
		},
		{
			Name:     "url",
			Prompt:   &survey.Input{Message: "Base URL", Default: "https://gitea.com"},
			Validate: survey.Required,
		},
		{
			Name:     "owner",
			Prompt:   &survey.Input{Message: "Owner Namespace", Help: "https://gitea.com/jolheiser/main -> jolheiser"},
			Validate: survey.Required,
		},
		{
			Name:     "branch",
			Prompt:   &survey.Input{Message: "New Default Branch Name", Default: "main"},
			Validate: survey.Required,
		},
	}

	answers := struct {
		Token  string
		URL    string
		Owner  string
		Branch string
	}{}

	if err := survey.Ask(questions, &answers); err != nil {
		return err
	}

	client, err := gitea.NewClient(answers.URL, gitea.SetToken(answers.Token))
	if err != nil {
		return err
	}

	repos, err := getRepos(client, answers.Owner, answers.Branch)
	if err != nil {
		return err
	}

	repoMap := make(map[string]*gitea.Repository)
	repoList := make([]string, len(repos))
	for idx, repo := range repos {
		repoMap[repo.Name] = repo
		repoList[idx] = repo.Name
	}

	question := &survey.MultiSelect{Message: "Repositories To Convert", Options: repoList}
	var answer []string
	if err := survey.AskOne(question, &answer); err != nil {
		return err
	}

	for _, repoName := range answer {
		beaver.Infof("Converting %s...", repoName)
		repo := repoMap[repoName]

		// Create new branch
		beaver.Debugf("[%s] Creating new branch %s", repoName, answers.Branch)
		_, _, err := client.CreateBranch(answers.Owner, repoName, gitea.CreateBranchOption{
			BranchName:    answers.Branch,
			OldBranchName: repo.DefaultBranch,
		})
		if err != nil {
			return err
		}

		// Retrieve old branch protection
		beaver.Debugf("[%s] Getting branch protection from %s", repoName, repo.DefaultBranch)
		foundProtection := true
		oldBranchProtection, _, err := client.GetBranchProtection(answers.Owner, repoName, repo.DefaultBranch)
		if err != nil {
			if !strings.EqualFold(err.Error(), "404 Not Found") {
				return err
			}
			foundProtection = false
			beaver.Debugf("No branch protection found for %s", repo.DefaultBranch)
		}

		if oldBranchProtection != nil && foundProtection {
			// Update new branch with same protection
			beaver.Debugf("[%s] Copying branch protection to %s", repoName, answers.Branch)
			_, _, err = client.CreateBranchProtection(answers.Owner, repoName, gitea.CreateBranchProtectionOption{
				BranchName:                  answers.Branch,
				EnablePush:                  oldBranchProtection.EnablePush,
				EnablePushWhitelist:         oldBranchProtection.EnablePushWhitelist,
				PushWhitelistUsernames:      oldBranchProtection.PushWhitelistUsernames,
				PushWhitelistTeams:          oldBranchProtection.PushWhitelistTeams,
				PushWhitelistDeployKeys:     oldBranchProtection.PushWhitelistDeployKeys,
				EnableMergeWhitelist:        oldBranchProtection.EnableMergeWhitelist,
				MergeWhitelistUsernames:     oldBranchProtection.MergeWhitelistUsernames,
				MergeWhitelistTeams:         oldBranchProtection.MergeWhitelistTeams,
				EnableStatusCheck:           oldBranchProtection.EnableStatusCheck,
				StatusCheckContexts:         oldBranchProtection.StatusCheckContexts,
				RequiredApprovals:           oldBranchProtection.RequiredApprovals,
				EnableApprovalsWhitelist:    oldBranchProtection.EnableApprovalsWhitelist,
				ApprovalsWhitelistUsernames: oldBranchProtection.ApprovalsWhitelistUsernames,
				ApprovalsWhitelistTeams:     oldBranchProtection.ApprovalsWhitelistTeams,
				BlockOnRejectedReviews:      oldBranchProtection.BlockOnRejectedReviews,
				BlockOnOutdatedBranch:       oldBranchProtection.BlockOnOutdatedBranch,
				DismissStaleApprovals:       oldBranchProtection.DismissStaleApprovals,
				RequireSignedCommits:        oldBranchProtection.RequireSignedCommits,
				ProtectedFilePatterns:       oldBranchProtection.ProtectedFilePatterns,
			})
			if err != nil {
				return err
			}

			// Remove old branch protection
			beaver.Debugf("[%s] Removing branch protection from %s", repoName, repo.DefaultBranch)
			_, err = client.DeleteBranchProtection(answers.Owner, repoName, repo.DefaultBranch)
			if err != nil {
				return err
			}
		}

		// Update default branch
		beaver.Debugf("[%s] Setting default branch to %s", repoName, answers.Branch)
		_, _, err = client.EditRepo(answers.Owner, repoName, gitea.EditRepoOption{
			DefaultBranch: &answers.Branch,
		})
		if err != nil {
			return err
		}

		// Update PRs
		prs, err := getPRs(client, answers.Owner, repoName, repo.DefaultBranch)
		if err != nil {
			return err
		}
		for _, pr := range prs {
			beaver.Debugf("[%s] Changing pull request target for PR #%d", repoName, pr.Index)
			_, _, err := client.EditPullRequest(answers.Owner, repoName, pr.Index, gitea.EditPullRequestOption{
				Base: answers.Branch,
			})
			if err != nil {
				return err
			}
		}

		// Remove old branch
		beaver.Debugf("[%s] Removing old branch %s", repoName, repo.DefaultBranch)
		_, _, err = client.DeleteRepoBranch(answers.Owner, repoName, repo.DefaultBranch)
		if err != nil {
			return err
		}
	}

	fmt.Println(color.Info.Format("Conversion completed for"), color.FgCyan.Formatf("%d", len(answer)), color.Info.Format("repositories."))
	return nil
}
