package main

import (
	"strings"

	"code.gitea.io/sdk/gitea"
	"go.jolheiser.com/beaver"
)

func getRepos(client *gitea.Client, owner, newBranch string) ([]*gitea.Repository, error) {
	repos := make([]*gitea.Repository, 0)
	p := 1
	for {
		beaver.Debugf("Getting repositories for %s - page %d", owner, p)
		opts := gitea.ListReposOptions{
			ListOptions: gitea.ListOptions{
				Page: p,
			},
		}
		list, _, err := client.ListUserRepos(owner, opts)
		if err != nil {
			return repos, err
		}
		p++

		// Filter
		for _, repo := range list {
			// Ignore forks, mirrors, and empty repos
			// Also ignore repos that already use the given branch as default
			if repo.Fork || repo.Mirror || repo.Empty || strings.EqualFold(repo.DefaultBranch, newBranch) {
				continue
			}
			repos = append(repos, repo)
		}

		if len(list) == 0 {
			break
		}
	}

	return repos, nil
}

func getPRs(client *gitea.Client, owner, repo, oldBranch string) ([]*gitea.PullRequest, error) {
	prs := make([]*gitea.PullRequest, 0)
	p := 1
	for {
		beaver.Debugf("[%s] Getting pull requests - page %d", repo, p)
		opts := gitea.ListPullRequestsOptions{
			State: gitea.StateOpen,
			ListOptions: gitea.ListOptions{
				Page: p,
			},
		}
		list, _, err := client.ListRepoPullRequests(owner, repo, opts)
		if err != nil {
			return prs, err
		}
		p++

		// Filter
		for _, pr := range list {
			if pr.Base == nil || !strings.EqualFold(pr.Base.Name, oldBranch) {
				continue
			}

			prs = append(prs, pr)
		}

		if len(list) == 0 {
			break
		}
	}

	return prs, nil
}
