# main

Batch update your Gitea repository default branches

* Migrate old default branch to new default branch
* Copy branch protections
* Change the target branch for all PRs that previously target old default branch

# License

[MIT](LICENSE)