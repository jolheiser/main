module go.jolheiser.com/main

go 1.15

require (
	code.gitea.io/sdk/gitea v0.13.0
	github.com/AlecAivazis/survey/v2 v2.1.1
	github.com/urfave/cli/v2 v2.2.0
	go.jolheiser.com/beaver v1.0.2
	golang.org/x/sys v0.0.0-20210216224549-f992740a1bac // indirect
)
